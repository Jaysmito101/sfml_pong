#include <SFML/Graphics.hpp>
#include <stdexcept>
#include <unordered_map>
#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <ctime>
#include <iostream>

#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 700

// NOTE: This is just a dummy function as we dont really need a
//        proepr UUID generator for this project but I just kept 
//        it here for the sake of completeness and also a placeholder
//        for a proper UUID generator in the future
static int GenerateUUID()
{
    static int uuid = 1;
    return uuid++;
}

static std::string s_OverScreenMessage = "";

// There should be proper header & source files for each of these classes
// but for the sake of simplicity and speed, I have just kept them all in one file
// Forward declarations
class Entity;
class Scene;
class InputSystem;

// This class is used a lot in other classes so the declaration is kept here
class GameManager
{
public:
    GameManager();
    ~GameManager();

    void Update();
    void Render(sf::RenderWindow &window);
    void ChangeScene(const std::string &name);
    inline void AddScene(std::shared_ptr<Scene> scene);
    inline bool IsRunning() const;
    void Quit();

private:
    sf::Clock m_InterFrameClock;
    float m_DeltaTime;
    std::string m_CurrentScene = "";
    bool m_IsRunning = true;
    std::unordered_map<std::string, std::shared_ptr<Scene>> m_Scenes;
};


class InputSystem
{
public:

    inline static void Init()
    {
        if (s_Instance != nullptr)
        {
            throw std::runtime_error("InputSystem already initialized");
        }
        s_Instance = new InputSystem();
    }

    inline static void Destroy()
    {
        if (s_Instance == nullptr)
        {
            throw std::runtime_error("InputSystem not initialized");
        }
        delete s_Instance;
    }

    inline static void UpdateWithEvent(sf::Event& event)
    {
        if (event.type == sf::Event::KeyPressed)
        {
            s_Instance->m_Keys[event.key.code] = true;
        }
        else if (event.type == sf::Event::KeyReleased)
        {
            s_Instance->m_Keys[event.key.code] = false;
        }
        else if (event.type == sf::Event::MouseMoved)
        {
            s_Instance->m_MousePosition = sf::Vector2f(event.mouseMove.x, event.mouseMove.y);
        }
        else if (event.type == sf::Event::MouseButtonPressed)
        {
            if (event.mouseButton.button == sf::Mouse::Left)
            {
                s_Instance->m_MouseLeftPressed = true;
            }
        }
        else if (event.type == sf::Event::MouseButtonReleased)
        {
            if (event.mouseButton.button == sf::Mouse::Left)
            {
                s_Instance->m_MouseLeftPressed = false;
            }
        }
    }

    inline static bool IsKeyPressed(sf::Keyboard::Key key)
    {
        return s_Instance->m_Keys[key];
    }

    inline static bool IsMouseLeftPressed()
    {
        return s_Instance->m_MouseLeftPressed;
    }

    inline static sf::Vector2f GetMousePosition()
    {
        return s_Instance->m_MousePosition;
    }

private:
    InputSystem()
    {
        for (int i = 0; i < sf::Keyboard::KeyCount; i++)
        {
            m_Keys[static_cast<sf::Keyboard::Key>(i)] = false;
        }
    }

    ~InputSystem()
    {
    }

    static InputSystem *s_Instance;
    std::unordered_map<sf::Keyboard::Key, bool> m_Keys;
    sf::Vector2f m_MousePosition;
    bool m_MouseLeftPressed = false;
};

InputSystem *InputSystem::s_Instance = nullptr;

class Entity
{
public:
    Entity(const std::string &name, const sf::Vector2f &position)
        : m_Name(name), m_Position(position)
    {
        m_Id = GenerateUUID();
    }

    virtual ~Entity()
    {
    }

    // These are not marked as virtual so as to make it optional to
    // override them in derived classes (say, we only need the render
    // and not the update function)
    virtual void OnUpdate(float deltaTime) {}
    virtual void OnRender(sf::RenderWindow &window) {}

    inline int GetId() const
    {
        return m_Id;
    }

    inline const std::string &GetName() const
    {
        return m_Name;
    }

    inline const sf::Vector2f &GetPosition() const
    {
        return m_Position;
    }

    inline void SetPosition(const sf::Vector2f &position)
    {
        m_Position = position;
    }

protected:
    int m_Id = -1;
    std::string m_Name = "";
    sf::Vector2f m_Position = { 0, 0 };
};

class Scene
{
public:
    Scene(const std::string &name)
        : m_Name(name)
    {
        m_Id = GenerateUUID();
    }

    virtual ~Scene()
    {
    }

    inline std::shared_ptr<Entity> AddEntity(std::shared_ptr<Entity> entity)
    {
        m_Entities.push_back(entity);
        return entity;
    }

    inline bool RemoveEntity(int id)
    {
        auto it = std::find_if(m_Entities.begin(), m_Entities.end(), [id](std::shared_ptr<Entity> entity) {
            return entity->GetId() == id;
        });

        if (it != m_Entities.end())
        {
            m_Entities.erase(it);
            return true;
        }

        return false;
    }

    inline const std::string &GetName() const
    {
        return m_Name;
    }

    inline int GetId() const
    {
        return m_Id;
    }

    inline const std::vector<std::shared_ptr<Entity>> &GetEntities() const
    {
        return m_Entities;
    }

    // These are not marked as virtual so as to make it optional to
    // override them in derived classes (say, we only need the render
    // and not the update function)
    virtual void OnUpdate(float deltaTime) {}
    virtual void OnRender(sf::RenderWindow &window) {}

    void Update(float deltaTime)
    {
        for (auto &entity : m_Entities)
        {
            entity->OnUpdate(deltaTime);
        }
     
        OnUpdate(deltaTime);
    }

    void Render(sf::RenderWindow &window)
    {
        OnRender(window);

        for (auto &entity : m_Entities)
        {
            entity->OnRender(window);
        }
    }


protected:
    std::string m_Name = "";
    std::vector<std::shared_ptr<Entity>> m_Entities;
    int m_Id = -1;
};

class ButtonEntity : public Entity
{
public:
    ButtonEntity(const std::string &name, const sf::Vector2f &position, const sf::Vector2f &size)
        : Entity(name, position), m_Size(size), m_OnClick(nullptr)
    {
        if (!m_Texture.loadFromFile("assets/button.png"))
        {
            throw std::runtime_error("Failed to load button image");
        }

        if (!m_Font.loadFromFile("assets/font.ttf"))
        {
            throw std::runtime_error("Failed to load font");
        }

        m_Sprite.setTexture(m_Texture);
        m_Sprite.setPosition(position);
        m_Sprite.setScale(
            size.x / m_Texture.getSize().x,
            size.y / m_Texture.getSize().y
        );

        m_Label.setString(name);
        m_Label.setCharacterSize(48);
        m_Label.setFont(m_Font);
        m_Label.setFillColor(sf::Color::Black);
        m_Label.setPosition(
            position.x + size.x / 2 - m_Label.getGlobalBounds().width / 2,
            position.y + size.y / 2 - m_Label.getGlobalBounds().height / 2
        );
        

    }

    virtual ~ButtonEntity()
    {
    }

    virtual void OnUpdate(float deltaTime) override
    {
        if (InputSystem::IsMouseLeftPressed())
        {
            auto mousePosition = InputSystem::GetMousePosition();
            if (mousePosition.x >= m_Position.x && mousePosition.x <= m_Position.x + m_Size.x &&
                mousePosition.y >= m_Position.y && mousePosition.y <= m_Position.y + m_Size.y)
            {
                if (m_OnClick != nullptr)
                {
                    m_OnClick();
                }
            }
        }
    }

    virtual void OnRender(sf::RenderWindow &window) override
    {
        window.draw(m_Sprite);
        window.draw(m_Label);
    }

    inline void SetOnClick(std::function<void()> onClick)
    {
        m_OnClick = onClick;
    }

private:
    sf::Texture m_Texture;
    sf::Sprite m_Sprite;
    sf::Vector2f m_Size;
    sf::Text m_Label;
    sf::Font m_Font;
    std::function<void()> m_OnClick;
};

class BallEntity : public Entity
{
public:
    BallEntity(const sf::Vector2f &position)
        : Entity("Ball", position)
    {
        m_InitPosition = position;
        m_Position = position;
        m_Speed = 300;

        m_Shape.setRadius(20);
        m_Shape.setFillColor(sf::Color::Red);

        this->Reset();
    }

    virtual ~BallEntity()
    {
    }

    void Reset()
    {
        m_Position = m_InitPosition;
        // randomize velocity (also make sure it is more horizontal than vertical otherwise it will be boring to play)
        m_Velocity = sf::Vector2f(static_cast<float>(3 * rand()) / RAND_MAX, static_cast<float>(rand()) / RAND_MAX);
        // normalize velocity
        m_Velocity /= sqrt(m_Velocity.x * m_Velocity.x + m_Velocity.y * m_Velocity.y);
    }

    virtual void OnUpdate(float deltaTime) override
    {
        m_Position = m_Position + m_Velocity * deltaTime * m_Speed;
        auto radius = m_Shape.getRadius();
        if (m_Position.x - radius <= 0)
        {
            m_Velocity.x = -m_Velocity.x;
            m_Position.x = radius;
        }
        else if (m_Position.x + radius >= WINDOW_WIDTH)
        {
            m_Velocity.x = -m_Velocity.x;
            m_Position.x = WINDOW_WIDTH - radius;
        }

        if (m_Position.y - radius <= 0)
        {
            m_Velocity.y = -m_Velocity.y;
            m_Position.y = radius;
        }
        else if (m_Position.y + radius >= WINDOW_HEIGHT)
        {
            m_Velocity.y = -m_Velocity.y;
            m_Position.y = WINDOW_HEIGHT - radius;
        }
    }

    inline void SetSpeed(float speed)
    {
        m_Speed = speed;
    }

    virtual void OnRender(sf::RenderWindow &window) override
    {
        m_Shape.setPosition(m_Position - sf::Vector2f(m_Shape.getRadius(), m_Shape.getRadius()));
        window.draw(m_Shape);
    }

    inline float GetRadius() const
    {
        return m_Shape.getRadius();
    }

    inline void InverseXVelocity()
    {
        m_Velocity.x = -m_Velocity.x;
    }

private:
    sf::CircleShape m_Shape;
    sf::Vector2f m_Velocity;
    sf::Vector2f m_InitPosition;
    float m_Speed;
};

class PlayerEntity : public Entity
{
public:
    PlayerEntity(const float xPosition, GameManager* gameManager, std::shared_ptr<BallEntity>& ball, std::pair<sf::Keyboard::Key, sf::Keyboard::Key> controls)
        : Entity("Player", sf::Vector2f(xPosition, static_cast<float>(WINDOW_HEIGHT) / 2)), m_GameManager(gameManager), m_Ball(ball)
    {
        if (!m_Font.loadFromFile("assets/font.ttf"))
        {
            throw std::runtime_error("Failed to load font");
        }

        m_Score = 0;

        m_ScoreText.setFont(m_Font);
        m_ScoreText.setString(std::to_string(m_Score));
        m_ScoreText.setCharacterSize(48);
        m_ScoreText.setFillColor(sf::Color::White);
        m_ScoreText.setPosition(
            xPosition < WINDOW_WIDTH / 2 ? 50 : WINDOW_WIDTH - 50 - m_ScoreText.getGlobalBounds().width,
            50
        );


        m_Shape.setSize(sf::Vector2f(20.f, 100.f));
        m_Shape.setFillColor(xPosition < WINDOW_WIDTH / 2 ? sf::Color::Blue : sf::Color::Green);
        m_UpKey = controls.first;
        m_DownKey = controls.second;
    }

    virtual void OnUpdate(float deltaTime) override
    {
        if (InputSystem::IsKeyPressed(m_UpKey))
        {
            m_Position.y -= 300 * deltaTime;
        }
        else if (InputSystem::IsKeyPressed(m_DownKey))
        {
            m_Position.y += 300 * deltaTime;
        }
        m_Position.y = std::clamp(m_Position.y, 50.f + m_Shape.getSize().y / 2, static_cast<float>(WINDOW_HEIGHT) - 50.f - m_Shape.getSize().y / 2);

        auto ballPosition = m_Ball->GetPosition();
        auto ballRadius = m_Ball->GetRadius();

        // check if ball player scores
        if ( (m_Position.x > WINDOW_WIDTH / 2 && ballPosition.x - ballRadius <= 0)
        || (m_Position.x < WINDOW_WIDTH / 2 && ballPosition.x + ballRadius >= WINDOW_WIDTH) )
        {
            m_Score++;
            m_ScoreText.setString(std::to_string(m_Score));
            m_Ball->Reset();
            
            if (m_Score == 5)
            {
                s_OverScreenMessage = m_Position.x < WINDOW_WIDTH / 2 ? "Player 1 wins!" : "Player 2 wins!";
                m_GameManager->ChangeScene("MainMenu");
            }
        }


        // check if ball hits player
        bool isColliding = ballPosition.x - ballRadius <= m_Position.x + m_Shape.getSize().x / 2 &&
            ballPosition.x + ballRadius >= m_Position.x - m_Shape.getSize().x / 2 &&
            ballPosition.y - ballRadius <= m_Position.y + m_Shape.getSize().y / 2 &&
            ballPosition.y + ballRadius >= m_Position.y - m_Shape.getSize().y / 2;

        if (!m_WasInTouch && isColliding)
        {
            m_Ball->InverseXVelocity();
            m_WasInTouch = true;
        }
        else if (!isColliding)
        {
            m_WasInTouch = false;
        }
    }

    virtual void OnRender(sf::RenderWindow& window) override
    {
        m_Shape.setPosition(m_Position - sf::Vector2f(m_Shape.getSize().x / 2, m_Shape.getSize().y / 2));
        window.draw(m_Shape);
        window.draw(m_ScoreText);
    }

private:
    sf::RectangleShape m_Shape;
    std::shared_ptr<BallEntity> m_Ball;
    sf::Keyboard::Key m_UpKey, m_DownKey;
    sf::Font m_Font;
    sf::Text m_ScoreText;
    int m_Score = 0;
    bool m_WasInTouch = false;
    GameManager* m_GameManager;
};

class MainMenuScene : public Scene
{
public:
    MainMenuScene(GameManager *gameManager)
        : Scene("Main"), m_GameManager(gameManager)
    {
        if (!m_Background.first.loadFromFile("assets/background.jpg"))
        {
            throw std::runtime_error("Failed to load background image");
        }

        if (!m_Font.loadFromFile("assets/font.ttf"))
        {
            throw std::runtime_error("Failed to load font");
        }

        m_Background.second.setTexture(m_Background.first);
        // If we make the window resizable, we need to move this to the OnUpdate function
        // also pass a reference to the window to main menu object
        m_Background.second.setScale(
            static_cast<float>(WINDOW_WIDTH) / m_Background.first.getSize().x,
            static_cast<float>(WINDOW_HEIGHT) / m_Background.first.getSize().y
        );

        m_WinnerText.setFont(m_Font);
        m_WinnerText.setString(s_OverScreenMessage);
        m_WinnerText.setCharacterSize(48);
        m_WinnerText.setFillColor(sf::Color::Black);
        m_WinnerText.setPosition(
            static_cast<float>(WINDOW_WIDTH) / 2 - m_WinnerText.getGlobalBounds().width / 2,
            100
        );

        auto playButton = std::static_pointer_cast<ButtonEntity>(this->AddEntity(std::make_shared<ButtonEntity>("Play", sf::Vector2f(static_cast<float>(WINDOW_WIDTH) / 2 - 200, 200), sf::Vector2f(400, 100))));
        playButton->SetOnClick([this]() {
            this->m_GameManager->ChangeScene("Game");
        });

        auto quitButton = std::static_pointer_cast<ButtonEntity>(this->AddEntity(std::make_shared<ButtonEntity>("Quit", sf::Vector2f(static_cast<float>(WINDOW_WIDTH) / 2 - 200, 350), sf::Vector2f(400, 100))));
        quitButton->SetOnClick([this]() {
            this->m_GameManager->Quit();
        });
    }

    virtual ~MainMenuScene()
    {
    }

    virtual void OnRender(sf::RenderWindow &window) override
    {
        // Draw the background fullscreen
        window.draw(m_Background.second);

        // This is basically reusing this main menu scene as gameover/winner scene
        if (s_OverScreenMessage.size() > 0)
        {
            m_WinnerText.setString(s_OverScreenMessage);
            m_WinnerText.setPosition(
                static_cast<float>(WINDOW_WIDTH) / 2 - m_WinnerText.getGlobalBounds().width / 2,
                100
            );
            window.draw(m_WinnerText);
        }
    }

private:
    GameManager *m_GameManager;
    std::pair<sf::Texture, sf::Sprite> m_Background;
    sf::Font m_Font;
    sf::Text m_WinnerText;
    // std::shared_ptr<ButtonEntity> m_PlayButton, m_QuitButton;
};

class GameScene : public Scene
{
public:
    GameScene(GameManager *gameManager)
        : Scene("Game"), m_GameManager(gameManager)
    {

        auto ball = std::static_pointer_cast<BallEntity>(this->AddEntity(std::make_shared<BallEntity>(sf::Vector2f(static_cast<float>(WINDOW_WIDTH) / 2, static_cast<float>(WINDOW_HEIGHT) / 2))));

        auto player1 = std::static_pointer_cast<PlayerEntity>(this->AddEntity(std::make_shared<PlayerEntity>(50, gameManager, ball, std::make_pair(sf::Keyboard::Q, sf::Keyboard::A))));
        auto player2 = std::static_pointer_cast<PlayerEntity>(this->AddEntity(std::make_shared<PlayerEntity>(WINDOW_WIDTH - 50, gameManager, ball, std::make_pair(sf::Keyboard::P, sf::Keyboard::L))));
    }

    void OnUpdate(float deltaTime) override
    {
    }

    virtual ~GameScene()
    {
    }

private:
    GameManager *m_GameManager = nullptr;
};


GameManager::GameManager()
{
    m_InterFrameClock.restart();

    // Add the Scenes
    m_Scenes["MainMenu"] = std::make_shared<MainMenuScene>(this);
    m_Scenes["Game"] = std::make_shared<GameScene>(this);

    m_CurrentScene = "MainMenu";
}

GameManager::~GameManager()
{
}

void GameManager::Update()
{
    m_DeltaTime = m_InterFrameClock.restart().asSeconds();

    if (m_CurrentScene == "")
    {
        throw std::runtime_error("No scene is set");
    }

    m_Scenes[m_CurrentScene]->Update(m_DeltaTime);
}

void GameManager::Render(sf::RenderWindow &window)
{
    if (m_CurrentScene == "")
    {
        throw std::runtime_error("No scene is set");
    }

    m_Scenes[m_CurrentScene]->Render(window);
}

void GameManager::ChangeScene(const std::string &name)
{
    if (m_Scenes.find(name) == m_Scenes.end())
    {
        throw std::runtime_error("Scene with name " + name + " does not exist");
    }

    m_CurrentScene = name;
}

inline void GameManager::AddScene(std::shared_ptr<Scene> scene)
{
    m_Scenes[scene->GetName()] = scene;
}

inline bool GameManager::IsRunning() const
{
    return m_IsRunning;
}

void GameManager::Quit()
{
    m_IsRunning = false;
}

int main()
{
    srand(time(NULL));
    auto window = sf::RenderWindow{ { WINDOW_WIDTH, WINDOW_HEIGHT }, "Pong - Jaysmito Mukherjee", sf::Style::Close };
    window.setFramerateLimit(30);

    GameManager gameManager;
    InputSystem::Init();

    while (window.isOpen() && gameManager.IsRunning())
    {


        for (auto event = sf::Event{}; window.pollEvent(event);)
        {
            InputSystem::UpdateWithEvent(event);

            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
        }

        gameManager.Update();

        window.clear(sf::Color::Black);
        gameManager.Render(window);
        window.display();
    }

    InputSystem::Destroy();
}